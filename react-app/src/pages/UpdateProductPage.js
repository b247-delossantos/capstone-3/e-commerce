import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Link, useParams } from 'react-router-dom';
import { useNavigate, Navigate } from 'react-router-dom';
import { Col } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function UpdateProductPage(){

 const { user, setUser, token } = useContext(UserContext);
 const [product, setProducts] = useState([]);
  const [productName, setProductName] = useState('');
  const [price, setProductPrice] = useState('');
  const [description, setProductDescription] = useState('');
  const [isActive, setIsActive] = useState(false);
  const navigate = useNavigate();
  const {courseId} = useParams();
 
  function updateProduct(e) {
    e.preventDefault();


    

    fetch(`${process.env.REACT_APP_API_URL}/products/${courseId}/update`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer '+ `${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        name: productName,
        price: price,
        description: description
      })
    })
    .then(res => res.json())
    .then(data => {

     if (data !== ""){
      console.log(data);
      console.log(courseId);

      Swal.fire({
        title: 'You have successfully updated a product!',
        icon: 'success',
        text: 'Thank you!'
      });
     navigate("/dashboard");
    } else {
      Swal.fire({
        title: "Something went wrong!",
        icon: 'error',
        text: 'Try again!'
      });
    }
  })

    setProductName("");
    setProductPrice("");
    setProductDescription("");
    setIsActive("");
  };


  useEffect(() => {
    if(productName !== "" && price !== "" && description !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [productName, price, description]);

return (
  <div className="container">
    <h1>Update Product</h1>
    <form onSubmit={(e) => updateProduct(e)}>
      <div className="form-group">
        <label htmlFor="productName">Product Name</label>
        <input
          type="text"
          className="form-control"
          id="productName"
          value={productName}
          onChange={e => setProductName(e.target.value)}
        />
      </div>

      <div className="form-group">
        <label htmlFor="productPrice">Product Price</label>
        <input
          type="text"
          className="form-control"
          id="productPrice"
          value={price}
          onChange={e => setProductPrice(e.target.value)}
        />
      </div>

      <div className="form-group">
        <label htmlFor="productDescription">Product Description</label>
        <textarea
          className="form-control"
          id="productDescription"
          value={description}
          onChange={e => setProductDescription(e.target.value)}
        />
      </div>

      <button type="submit" className="btn btn-primary" disabled={!isActive}>Update Product</button>
      <Link to={`/dashboard`} className="btn btn-link">Cancel</Link>
    </form>
  </div>
);
};