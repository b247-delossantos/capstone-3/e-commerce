import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Link } from 'react-router-dom';
import { useNavigate, Navigate } from 'react-router-dom';
import { Col } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function CreateProductPage() {
  const [productName, setProductName] = useState('');
  const [price, setProductPrice] = useState('');
  const [description, setProductDescription] = useState('');
  const [isActive, setIsActive] = useState(false);
  const {token} = useContext(UserContext);
  


  function addProduct(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/addproduct`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer '+ `${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        productName: productName,
        description: description,
        price: price
      })
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);
        if (data !== "") {
         
          Swal.fire({
            title: 'Product Added!',
            icon: 'success',
            text: 'You can add more product'
          });
        } else {
          Swal.fire({
            title: 'Something went wrong!',
            icon: 'error',
            text: 'Try again'
          });
        }
      })
      .catch(error => {
        console.error('Error adding product:', error);
        Swal.fire({
          title: "Something went wrong!",
          icon: 'error',
          text: 'Try again'
        });
      });

    setProductName('');
    setProductPrice('');
    setProductDescription('');
    setIsActive(false);
  }

  useEffect(() => {
    if (productName !== '' && price !== '' && description !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [productName, price, description]);

  return (
    <div className="container">
      <h1>Create Product</h1>
      <form onSubmit={e => addProduct(e)}>
        <div className="form-group">
          <label>Product Name</label>
          <input
            type="text"
            className="form-control"
            id="productName"
            value={productName}
            onChange={e => setProductName(e.target.value)}
          />
        </div>

        <div className="form-group">
          <label>Product Price</label>
          <input
            type="text"
            className="form-control"
            id="productPrice"
            value={price}
            onChange={e => setProductPrice(e.target.value)}
          />
        </div>

        <div className="form-group">
          <label>Product Description</label>
          <textarea
            className="form-control"
            id="productDescription"
            value={description}
            onChange={e => setProductDescription(e.target.value)}
          />
        </div>

        <button type="submit" className="btn btn-primary" disabled={!isActive}>
          Create Product
        </button>
      </form>
    </div>
  );
}
