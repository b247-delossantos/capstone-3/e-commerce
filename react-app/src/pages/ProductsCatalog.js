import React, { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

function ProductsCatalog() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/active`)
      .then(response => response.json())
      .then(data => setProducts(data))
      .catch(error => console.error(error));
  }, []);

  const addToCart = (productId) => {
    // TODO: Implement cart functionality
    console.log(`Product ${productId} added to cart`);
  }

  return (
    <div>
      <h1>Products Catalog</h1>
      {products.map(product => (
        <Card key={product._id}>
          <Card.Img variant="top" src={product.image} alt={product.productName} />
          <Card.Body>
            <Card.Title>{product.productName}</Card.Title>
            <Card.Text>{product.description}</Card.Text>
            <Link to={`/products/${product._id}`}>
              <Button variant="primary">View details</Button>
            </Link>
            <Button variant="primary" onClick={() => addToCart(product._id)}>Add to cart</Button>
          </Card.Body>
        </Card>
      ))}
    </div>
  );
}

export default ProductsCatalog;