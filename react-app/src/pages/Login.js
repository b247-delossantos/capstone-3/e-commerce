

import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Link } from 'react-router-dom';
import { useNavigate, Navigate } from 'react-router-dom';
import { Col } from 'react-bootstrap';



import Swal from 'sweetalert2';

export default function Login(){

	const { user, setUser } = useContext(UserContext);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isActive, setIsActive] = useState("true")

	function authenticate(e) {

		e.preventDefault();

		

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			// Sets the headers data of the 'request' object to be sent to the backend
			headers: {
				'Content-Type': 'application/json'
			},
			// JSON.stringify converts object data into stringified JSON
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		// The first ".then" method from the "response" object to convert the data retrieved into JSON format to be used in our application
		.then(res => res.json())
		.then(data => {

			// We will received either a token or an error response
			console.log(data.access)

			// If no user information is found, the 'access' property will not be available and will return undefiend
			// Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type
			if(typeof data.access !== "undefined"){

				// The JWT will be used to retrieved user information across the whole frontend application and stroing it in the localStorage will allow ease of access to the user's information

				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: 'Maligayang Araw! Matagumpay!',
					icon: 'success',
					text: 'Ikintutuwa kong makita ka!'
				})
			} else {
				Swal.fire({
					title: "Yun lang, mali!",
					icon: 'error',
					text: 'Siguraduhing tama ito!!'
				})
			}
		});

		// Set the email of the authenticated user in the local storage
			// Syntax
			// localStorage.setItem('propertyName', value)
		//localStorage.setItem("email", email);

		// In this case, the key name is 'email' and the value is the value of the email variable. This code sets the value of 'email' key in the local storage to the value of the email variable.

		//setUser( { email: localStorage.getItem('email')} );

		setEmail("");
		setPassword("");
		//navigate('/');

		//alert(`"${email}" has been verified! Welcome back!`);
	};

	const retrieveUserDetails = (token) => {

		// The token will be sent as part of the request's header information 
		// We put "Bearer" in front of the token to follow implementation standards for JWTs
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			// Global user state for validation accross the whole app
			// Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			});
		});
	};



useEffect(() => {
		if(email !== "" && password !== "") {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password]);

	if (user.id !== null) {
		if (user.isAdmin) {
			return <Navigate to="/dashboard" />;
		} else {
			return <Navigate to="/courses" />;
		}
	}

	return (
		<Form onSubmit={(e) => authenticate(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email here"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
					required
				/>
			</Form.Group>
			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password}
					onChange={(e) => setPassword(e.target.value)}
					required
				/>
			</Form.Group>
			<div style={{ textAlign: 'center' }}>
			{ isActive ? 
				<Col xs={12} md={12} lg={12} sm={12}>
				<Button variant="primary my-3" type="submit" id="submitBtn" className={"w-100"}>Submit</Button>
				</Col>
				: 
				<Col xs={12} md={12} lg={12} sm={12}>
				<Button variant="danger my-3" type="submit" id="submitBtn" className={"w-100"} disabled>Submit</Button>
				</Col>
			}	

			<Form.Group>
				<Form.Text>
				Don't have an account yet? <Link to="/register">Click here</Link> to register.
				</Form.Text>
			</Form.Group>
			</div>
		</Form>
	)
}