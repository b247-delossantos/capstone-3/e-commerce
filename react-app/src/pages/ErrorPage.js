/*import React from "react";
import Banner from '../components/Banner';
import Link from 'react-router-dom';




const ErrorPage = () => {
  return (
  <>
  <h3>Zuitt Booking</h3>
  <h1>404 Error: Page not found</h1>
   <p>Go back to the <a href="/">homepage</a></p>
  </>
);

};

export default ErrorPage;
*/
import Banner from '../components/Banner'

export default function Error(){

  const data = {
    title: "Error 404 - Page Not Found!",
    content: "The page you are looking for cannot be found",
    destination: "/",
    label: "Back to Home"
  }

  return (
    <Banner data={data} />
  )
}
