import Banner from '../components/Banner';
import Highlight from '../components/Highlight';

export default function Home(){

	const data = {
		title: "PreLovedBooks"
	}
	
	return (
		<>
			<Banner data={data} />
			<Highlight />
		</>
	)
}
