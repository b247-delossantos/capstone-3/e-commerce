/*import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import Swal from 'sweetalert2';


export default function AdminDashboardPage(){

  const {productName, setproductName} = useState("");
  const {description, setDescription} = useState("");
  const {price, setPrice} = useState("");

  function addProduct(e){

    fetch(`${process.env.REACT_APP_API_URL}/products/addproduct`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        productName: productName,
        description: description,
        price: price
      })
    })
    .then(res => res.json())
    .then(data => {

      if(data) {

        Swal.fire({
          title: "Registration Successful!",
          icon: "success",
          text: "You have added a Product!"
        })

        setproductName("");
        setDescription("");
        setPrice("");

      } else {

        Swal.fire({

          title: "Something went wrong!",
          icon: "error",
          text: "Please try again!"
        })
      }
    })
  }

useEffect(() => {

  if((productName !== "" && description !== "" && price !== "" )){
    
  }

  }, [ productName, description, price]);

  return(
    <Form>
        <Form.Group controlId="productName" className="my-2">
              <Form.Label>Product Name</Form.Label>
                <Form.Control 
                  type="text" 
                  placeholder="Enter Product Name"
                  value = {productName}
                  onChange = {e => setproductName(e.target.value)} 
                  required
                /> 
            </Form.Group>
            <Form.Group controlId="description" className="my-2">
              <Form.Label>Description</Form.Label>
                <Form.Control 
                  type="text" 
                  placeholder="Enter description"
                  value = {description}
                  onChange = {e => setDescription(e.target.value)} 
                  required
                /> 
            </Form.Group>
            <Form.Group controlId="price" className="my-2">
              <Form.Label>Price</Form.Label>
                <Form.Control 
                  type="text" 
                  placeholder="Enter price"
                  value = {price}
                  onChange = {e => setPrice(e.target.value)} 
                  required
                /> 
            </Form.Group>


  </Form>
  )
*/

    


/*useEffect(() => {
    if(productName !== "" && description !== "" && price !== ""){
      message: "You have successfully created a product!"
    }else {
      message: "Input the necessary information!"
    }
  }, [productName, description, price]);

return(
  <Form>
        <Form.Group controlId="productName" className="my-2">
              <Form.Label>Product Name</Form.Label>
                <Form.Control 
                  type="text" 
                  placeholder="Enter Product Name"
                  value = {productName}
                  onChange = {e => setproductName(e.target.value)} 
                  required
                /> 
            </Form.Group>
            <Form.Group controlId="description" className="my-2">
              <Form.Label>Description</Form.Label>
                <Form.Control 
                  type="text" 
                  placeholder="Enter description"
                  value = {description}
                  onChange = {e => setDescription(e.target.value)} 
                  required
                /> 
            </Form.Group>
            <Form.Group controlId="price" className="my-2">
              <Form.Label>Price</Form.Label>
                <Form.Control 
                  type="text" 
                  placeholder="Enter price"
                  value = {price}
                  onChange = {e => setPrice(e.target.value)} 
                  required
                /> 
            </Form.Group>


  </Form>
  )
};


*/

import React, { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import { Link, useParams } from 'react-router-dom';
import { Button } from 'react-bootstrap';

function MyProducts() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`) // replace with your API endpoint
      .then(response => response.json())
      .then(data => setProducts(data))
      .catch(error => console.error(error));
  }, []);

  const handleActivate = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activate`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(response => response.json())
      .then(data => {
        if (data.message) {
          console.log(data.message);
        } else {
          // update the products state with the updated product
          const updatedProducts = products.map(product => {
            if (product._id === productId) {
              return { ...product, isActive: true };
            } else {
              return product;
            }
          });
          setProducts(updatedProducts);
        }
      })
      .catch(error => console.error(error));
  };

  const handleDeactivate = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(response => response.json())
      .then(data => {
        if (data.message) {
          console.log(data.message);
        } else {
          // update the products state with the updated product
          const updatedProducts = products.map(product => {
            if (product._id === productId) {
              return { ...product, isActive: false };
            } else {
              return product;
            }
          });
          setProducts(updatedProducts);
        }
      })
      .catch(error => console.error(error));
  };

  return (
    <div>
      <h1>My Products</h1>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Active</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {products.map(product => (
            <tr key={product._id}>
              <td>{product._id}</td>
              <td>{product.productName}</td>
              <td>{product.description}</td>
              <td>{product.price}</td>
              <td>{product.isActive ? 'Yes' : 'No'}</td>
              <td>
                <Link to={`/updateproduct`}>
                  <Button variant="primary">Update</Button>
                </Link>
                {product.isActive ?
                  <Button variant="warning" onClick={() => handleDeactivate(product._id)}>Deactivate</Button>
                  :
                  <Button variant="success" onClick={() => handleActivate(product._id)}>Activate</Button>
                }
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
}

export default MyProducts;

