import React from 'react';

const AppFooter = () => {
  return (
    <footer style={{ backgroundColor: 'gray', color: 'white', textAlign: 'center', position: 'absolute', bottom: 0, width: '100%', height: '60px'}}>
      This is the footer.
    </footer>
  );
};

export default AppFooter;
