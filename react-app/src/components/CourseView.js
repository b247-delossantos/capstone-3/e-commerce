import { useEffect, useState, useContext } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Container, Row, Col, Card, Button, Spinner } from 'react-bootstrap';
import { FaShoppingCart } from 'react-icons/fa';
import { useNavigate, Navigate } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function CourseView() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const { courseId } = useParams();
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [enrollees, setEnrollees] = useState(0);
  const enroll = (courseId) => {
    fetch(`${process.env.REACT_APP_API_URL}/order/checkout`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({ products: [{ productId: courseId, quantity: 1 }], totalAmount: price }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: 'Successfully Checkout',
            icon: 'success',
            text: 'You have successfully checkout this product.',
          });

          setEnrollees((prevEnrollees) => prevEnrollees + 1);
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please try again',
          });
        }
      });
  };
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${courseId}/details`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data.enrollees);

        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setEnrollees(data.enrollees.length);
      });
  }, [courseId]);

  return (
    <Container className="py-4">
      <Row className="justify-content-md-center">
        <Col md={8}>
          <Card className="shadow-sm border-0">
            <Card.Body className="text-center">
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle className="mb-3 text-muted">Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle className="mt-3 mb-2 text-muted">Price:</Card.Subtitle>
              <Card.Text className="mb-4">PhP {price}</Card.Text>

              {user.id !== null ? (
                <>
                  <Card.Subtitle className="mt-3 mb-2 text-muted">Sold:</Card.Subtitle>
                  <Card.Text className="mb-3">{enrollees}</Card.Text>
                  <Button variant="primary" onClick={() => enroll(courseId)}>
                    <FaShoppingCart className="mr-2" />
                    Checkout
                  </Button>
                </>
              ) : (
                <Button className="btn btn-danger" as={Link} to="/login">
                  Log in to Shop
                </Button>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
