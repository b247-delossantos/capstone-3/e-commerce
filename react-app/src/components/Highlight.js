import { Card, Row, Col } from 'react-bootstrap';

export default function HighlightPage() {
  return (
    <Row className="mt-5">
      <Col xs={12} md={4} className="mb-4">
        <Card className="bg-light shadow-sm rounded-3">
          <Card.Body>
            <Card.Title className="mb-4">
              <h2>Find Your Next Read</h2>
            </Card.Title>
            <Card.Text>
              Browse our selection of secondhand books to find your next
              favorite read. From classics to contemporary bestsellers, we
              have something for every book lover.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={12} md={4} className="mb-4">
        <Card className="bg-light shadow-sm rounded-3">
          <Card.Body>
            <Card.Title className="mb-4">
              <h2>Save Money</h2>
            </Card.Title>
            <Card.Text>
              Our secondhand books are priced affordably, so you can stock up
              on your reading list without breaking the bank. Plus, buying
              used is good for the environment!
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={12} md={4} className="mb-4">
        <Card className="bg-light shadow-sm rounded-3">
          <Card.Body>
            <Card.Title className="mb-4">
              <h2>Join Our Community</h2>
            </Card.Title>
            <Card.Text>
              Connect with fellow book lovers and sellers in our community.
              Leave reviews, share recommendations, and discover new reads
              together.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
