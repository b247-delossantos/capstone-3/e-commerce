import React, { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';


function ProductView(productId) {
  const [product, setProduct] = useState(null);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/details`)
      .then(response => response.json())
      .then(data => setProduct(data))
      .catch(error => console.error(error));
  }, [productId]);

  const addToCart = () => {
    // TODO: Implement cart functionality
    console.log(`Product ${product._id} added to cart`);
  }

  return (
    <div>
      {product ? (
        <Card>
          <Card.Img variant="top" src={product.image} alt={product.productName} />
          <Card.Body>
            <Card.Title>{product.productName}</Card.Title>
            <Card.Text>{product.description}</Card.Text>
            <Button variant="primary" onClick={addToCart}>Add to cart</Button>
          </Card.Body>
        </Card>
      ) : (
        <p>Loading...</p>
      )}
    </div>
  );
}

export default ProductView;