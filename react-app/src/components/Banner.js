import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';

import { Link } from 'react-router-dom';

export default function Banner({data}) {

  const { title, content, destination, label } = data;

  return (
    <Container fluid className="p-0">
      <Row className="justify-content-center align-items-center">
        <Col md={6} className="text-center text-md-start">
          <h1 className="display-4" style={{ color: '#0d6efd', fontWeight: 'bold' }}>{title}</h1>
          <p className="lead mb-4" style={{ fontSize: '1.25rem' }}>{content}</p>
          <Button variant="primary" as={Link} to="/courses" className="mb-3">Shop Now!</Button>
          <p className="text-muted"><small>Free shipping on all orders over P500</small></p>
        </Col>
        <Col md={6} className="d-none d-md-block">
          <img src="https://images.ctfassets.net/17o2epk8ivh7/i5VpiX9YFwult8Su7DTOT/85878292aa07eb5c3e2b09124bd7b47e/10_BOOKS.jpg?h=1215&q=90&w=2160" alt="Banner" className="img-fluid" />
        </Col>
      </Row>
    </Container>
  );
}