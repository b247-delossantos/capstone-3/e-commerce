import { useEffect, useState, useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import UserContext from '../UserContext';
import { Container, Row, Col } from 'react-bootstrap';

export default function AppNavbar() {
  const { user } = useContext(UserContext);

  return (
    <>
      <Navbar bg="light" expand="lg">
        <Navbar.Brand as={Link} to="/" className="ml-auto p-3" style={{ fontWeight: 'bold', color: 'purple' }}>
          PLB
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto mx-auto">
            <Nav.Link as={NavLink} to="/" style={{ fontWeight: 'bold' }}>
              Home
            </Nav.Link>
            <Nav.Link as={NavLink} to="/courses" style={{ fontWeight: 'bold' }}>
              Shop
            </Nav.Link>
            {user.isAdmin ? (
              <NavDropdown title="Dashboard" id="basic-nav-dropdown">
                <NavDropdown.Item as={NavLink} to="/dashboard" style={{ fontWeight: 'bold' }}>
                  Dashboard
                </NavDropdown.Item>
                <NavDropdown.Item as={NavLink} to="/createproduct" style={{ fontWeight: 'bold' }}>
                  Create Product
                </NavDropdown.Item>
                <NavDropdown.Item as={NavLink} to="/users" style={{ fontWeight: 'bold' }}>
                  Users
                </NavDropdown.Item>
              </NavDropdown>
            ) : null}
            {user.id !== null ? (
              <Nav.Link as={NavLink} to="/logout" style={{ fontWeight: 'bold' }}>
                Logout
              </Nav.Link>
            ) : (
              <>
                <Nav.Link as={NavLink} to="/login" style={{ fontWeight: 'bold' }}>
                  Login
                </Nav.Link>
                <Nav.Link as={NavLink} to="/register" style={{ fontWeight: 'bold' }}>
                  Register
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <footer className="bg-light fixed-bottom">
        <Container>
          <Row>
            <Col>
              <p>This is a sample footer.</p>
            </Col>
          </Row>
        </Container>
      </footer>
    </>
  );
}
