import PropTypes from 'prop-types';
import { Card, Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({ course }) {
  const { name, description, price, image, _id } = course;

  return (
    <Card className="my-3">
      <Row>
        <Col md={4}>
          <Card.Img variant="top" src={image} alt={name} />
        </Col>
        <Col md={8}>
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price</Card.Subtitle>
            <Card.Text>{price}</Card.Text>
            <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>
              Details
            </Button>
          </Card.Body>
        </Col>
      </Row>
    </Card>
  );
}

CourseCard.propTypes = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
  }),
};